resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = lower(replace(var.name, "/\\s/", "-"))
  replication_group_description = "${var.name} replication group"
  number_cache_clusters         = var.redis_clusters
  node_type                     = var.node_type
  port                          = var.port
  automatic_failover_enabled    = var.redis_clusters > 1 ? true : false
  engine_version                = var.redis_version
  at_rest_encryption_enabled    = var.kms_key_arn != null ? true : var.at_rest_encryption_enabled
  kms_key_id                    = var.kms_key_arn
  auth_token                    = var.auth_token
  transit_encryption_enabled    = var.auth_token != null ? true : var.transit_encryption_enabled
  subnet_group_name             = aws_elasticache_subnet_group.redis_subnet_group.id
  security_group_ids            = compact(concat([aws_security_group.redis_default_sg.id], var.additional_security_group_ids))
  apply_immediately             = var.apply_immediately
  tags                          = merge(local.common_tags, var.extra_tags)

  lifecycle {
    ignore_changes = [
      engine_version
    ]
  }

}

resource "aws_elasticache_subnet_group" "redis_subnet_group" {
  name        = lower(replace(var.name, "/\\s/", "-"))
  description = "${lower(var.name)} subnet group"
  subnet_ids  = var.subnet_ids
}

resource "aws_security_group" "redis_default_sg" {
  name        = "${lower(replace(var.name, "/\\s/", "-"))}-elasticache-redis"
  description = "${var.name} Default Redis Security Group"
  vpc_id      = var.vpc_id
  tags = merge(
    {
      Name = "${var.name} ElastiCache Redis"
    },
    local.common_tags,
    var.extra_tags
  )
}

//resource "aws_cloudwatch_metric_alarm" "cache_cpu" {
//  count = "${var.redis_clusters}"
//
//  alarm_name          = "alarm-${var.name}-CacheCluster00${count.index + 1}CPUUtilization"
//  alarm_description   = "Redis cluster CPU utilization"
//  comparison_operator = "GreaterThanThreshold"
//  evaluation_periods  = "1"
//  metric_name         = "CPUUtilization"
//  namespace           = "AWS/ElastiCache"
//  period              = "300"
//  statistic           = "Average"
//
//  threshold = "${var.alarm_cpu_threshold}"
//
//  dimensions {
//    CacheClusterId = "${aws_elasticache_replication_group.redis.id}-00${count.index + 1}"
//  }
//
//  alarm_actions = ["${var.alarm_actions}"]
//}
//
//resource "aws_cloudwatch_metric_alarm" "cache_memory" {
//  count = "${var.redis_clusters}"
//
//  alarm_name          = "alarm-${var.name}-CacheCluster00${count.index + 1}FreeableMemory"
//  alarm_description   = "Redis cluster freeable memory"
//  comparison_operator = "LessThanThreshold"
//  evaluation_periods  = "1"
//  metric_name         = "FreeableMemory"
//  namespace           = "AWS/ElastiCache"
//  period              = "60"
//  statistic           = "Average"
//
//  threshold = "${var.alarm_memory_threshold}"
//
//  dimensions {
//    CacheClusterId = "${aws_elasticache_replication_group.redis.id}-00${count.index + 1}"
//  }
//
//  alarm_actions = ["${var.alarm_actions}"]
//}
