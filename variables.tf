variable "name" {
  description = "Common name, a unique identifier"
  type        = string
}

variable "redis_version" {
  description = "Redis version to use"
  type        = string
  default     = "6.x"
}

variable "node_type" {
  description = "Instance type to use for creating the Redis cache clusters"
  type        = string
  default     = "cache.t3.micro"
}

variable "redis_clusters" {
  description = "Number of Redis cache clusters (nodes) to create"
  type        = number
  default     = 1
}

variable "vpc_id" {
  description = "ID of a pre-existing VPC"
  type        = string
}

variable "subnet_ids" {
  description = "Subnet IDs to attach ALB to"
  type        = list(string)
}

variable "port" {
  description = ""
  type        = number
  default     = 6379
}

variable "auth_token" {
  description = "Automatically turns on encryption on transit"
  type        = string
  default     = null
}

variable "at_rest_encryption_enabled" {
  description = "Whether or not storage encryption must be turned on"
  type        = bool
  default     = true
}

variable "transit_encryption_enabled" {
  description = "Whether or not tansport encryption must be turned on"
  type        = bool
  default     = false
}

variable "kms_key_arn" {
  description = ""
  type        = string
  default     = null
}

//variable "alarm_cpu_threshold" {
//  default = "75"
//}
//
//variable "alarm_memory_threshold" {
//  default = "10000000" /* 10 MB */
//}
//
//variable "alarm_actions" {
//  type    = "list"
//  default = []
//}

variable "apply_immediately" {
  description = "Whether to apply changes immediately, or during the next maintenance window"
  type        = bool
  default     = false
}

variable "additional_security_group_ids" {
  description = "Any additional Security Group IDs to attach to the service"
  type        = list(string)

  validation {
    condition     = length(var.additional_security_group_ids) <= 4
    error_message = "Only 5 security groups are allowed per network interface. One has been already taken by the module."
  }

  default = []
}

variable "extra_tags" {
  description = "Any extra tags to apply to dirrefent resources"
  type        = map(string)
  default     = {}
}
