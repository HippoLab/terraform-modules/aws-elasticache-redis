AWS ElastiCache Redis
=====================

Module creates following AWS resources:

- Elasticache replication group
- Elasticache subnet group
- Associated Security Group
- Number of CloudWatch alarms

# Contents
- [Required Input Variables](#variables)
- [Usage](#usage)
- [Outputs](#outputs)
- [Licence](#licence)
- [Author Information](#author)

## <a name="variables"></a> Required Input Variables
At least following input variables must be provided. See [full list](variables.tf) of supported variables

| Name           | Description                             |
| -------------- | --------------------------------------- |
| name           | Common name - unique identifier         |
| vpc_id         | ID of a VPC resource will be created in |
| subnet_ids     | Subnet IDs service will be attached to  |

## <a name="usage"></a> Usage
```hcl
module "network" {
  source     = "git::https://gitlab.com/HippoLab/terraform-modules/aws-network.git"
  name       = "${var.project_name} ${var.environment}"
  extra_tags = local.common_tags
}

resource "aws_kms_key" "environment_kms_key" {
  description = "${var.project_name} ${local.environment} KMS key"
  policy      = data.aws_iam_policy_document.environment_kms_key.json
  tags        = local.common_tags
}

resource "aws_kms_alias" "environment_kms_key" {
  name          = "alias/${lower(join("-", [var.project_name, local.environment]))}"
  target_key_id = aws_kms_key.environment_kms_key.key_id
}

module "redis" {
  source      = "git::https://gitlab.com/HippoLab/terraform-modules/aws-elasticache-redis.git"
  name        = "${var.project_name} ${local.environment}"
  subnet_ids  = module.network.subnet_ids["private"]
  vpc_id      = module.network.vpc_id
  auth_token  = random_password.redis_auth_token.result
  kms_key_arn = aws_kms_key.environment_kms_key.arn
  extra_tags  = local.common_tags
}

resource "aws_secretsmanager_secret" "redis_auth_token" {
  name                    = "/${lower(var.project_name)}/${lower(local.environment)}/redis_auth_token"
  recovery_window_in_days = local.environment == "production" ? 7 : 0
  description             = "${var.project_name} ${local.environment} Redis Auth Token"
  kms_key_id              = aws_kms_key.environment_kms_key.arn
  tags                    = local.common_tags
}

resource "aws_secretsmanager_secret_version" "redis_auth_token" {
  secret_id     = aws_secretsmanager_secret.redis_auth_token.id
  secret_string = random_password.redis_auth_token.result
}

resource "random_password" "redis_auth_token" {
  length  = 64
  upper   = true
  lower   = true
  number  = true
  special = false
}
```

## <a name="outputs"></a> Outputs
Full list of module outputs and their descriptions can be found in [outputs.tf](outputs.tf)

## <a name="licence"></a> Licence
The module is distributed under [MIT Licence](LICENCE.txt). Please make sure you have read, understood and agreed
to its terms and conditions

## <a name="author"></a> Author Information
Vladimir Tiukhtin <vladimir.tiukhtin@hippolab.ru><br/>London
